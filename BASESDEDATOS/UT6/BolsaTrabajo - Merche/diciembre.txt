INTERCAMBIOS EUROPEOS
Se abre el plazo para convocar a los centros europeos que deseen participar en actividades Europeas con centros espa�oles de Educaci�n Secundaria. Formaran parte de las actividades tanto alumnos como profesores, quedando subvencionado parte del programa y el viaje.

RIESGOS DE LAS REDES SOCIALES
Actividad enmarcada dentro de las jornadas de seguridad y protecci�n al menor. Dirigidas a estudiantes de educaci�n secundaria, bachillerato, ciclos formativos y familias. Plazas limitadas. Se entregar� diploma de asistencia. Lugar Aula Magna del colegio salesiano ntra. sra. del Pilar de Zaragoza

TALLER:E-LEARNING
Segundas jornadas del taller e-learning. Este taller va dirigido a personas con inquietudes emprendedoras y con una o varias ideas de negocio en mente. El objetivo de la actividad es que los asistentes conozcan la herramientas necesarias para testar su idea de negocio, principalmente el LEAN CANVAS, y que las pongan en pr�cticas a trav�s de ejercicios o experiencias reales.

